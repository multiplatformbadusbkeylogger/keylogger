module keylogger

go 1.21.3

require (
	github.com/TheTitanrain/w32 v0.0.0-20200114052255-2654d97dbd3d
	github.com/gorilla/websocket v1.5.1
	github.com/zcalusic/sysinfo v1.0.2
)

require (
	github.com/google/uuid v1.3.1 // indirect
	golang.org/x/net v0.17.0 // indirect
)
